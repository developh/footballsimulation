<?php


namespace App\Contracts;


interface MatchInterface extends BaseInterface
{
    /**
     * Get current match
     * @return mixed
     */
    public function currentWeek();

    /**
     * Update match
     * @param array $data
     * @return boolean
     */
    public function update($data =[]);
}
