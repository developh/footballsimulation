<?php


namespace App\Contracts;


interface SimulationInterface
{
    /**
     * Play week by week
     * @return boolean
     */
    public function playWeek();

    /**
     * Play all league matches
     * @return boolean
     */
    public function playAll();

}
