<?php

namespace App\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Entities\Match
 *
 * @method static Builder|Match newModelQuery()
 * @method static Builder|Match newQuery()
 * @method static Builder|Match query()
 * @mixin Eloquent
 * @property int $id
 * @property int $home_team
 * @property int|null $home_score
 * @property int $away_team
 * @property int|null $away_score
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Match whereAwayScore($value)
 * @method static Builder|Match whereAwayTeam($value)
 * @method static Builder|Match whereCreatedAt($value)
 * @method static Builder|Match whereHomeScore($value)
 * @method static Builder|Match whereHomeTeam($value)
 * @method static Builder|Match whereId($value)
 * @method static Builder|Match whereUpdatedAt($value)
 * @property-read Team $away
 * @property-read Team $home
 * @property int $week_number
 * @method static Builder|Match whereWeekNumber($value)
 */
class Match extends Model
{
    protected $fillable = [
        'home_team',
        'home_score',
        'away_team',
        'away_score',
        'week_number',
    ];

    /**
     * @return BelongsTo
     */
    function home(){
        return $this->belongsTo(Team::class,'home_team');
    }

    /**
     * @return BelongsTo
     */
    function away(){
        return $this->belongsTo(Team::class,'away_team');
    }
}
