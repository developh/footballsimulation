<?php

namespace App\Entities;


use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Entities\Team
 *
 * @property-read Collection|Match[] $matches
 * @method static Builder|Team newModelQuery()
 * @method static Builder|Team newQuery()
 * @method static Builder|Team query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Team whereCreatedAt($value)
 * @method static Builder|Team whereId($value)
 * @method static Builder|Team whereName($value)
 * @method static Builder|Team whereUpdatedAt($value)
 * @property-read Collection|Match[] $homeMatches
 * @property-read Collection|Match[] $awayMatches
 * @property-read int $goals_against
 * @property-read int $goals_for
 * @property-read int $total_drawn
 * @property-read int $total_loss
 * @property-read int $total_match
 * @property-read mixed $total_points
 * @property-read int $total_win
 */
class Team extends Eloquent
{
    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'total_match',
        'total_points',
        'total_win',
        'total_drawn',
        'total_loss',
        'goals_for',
        'goals_against',
    ];

    /**
     * Make relation with matches table to get all home matches result from there
     * @return HasMany
     */
    function homeMatches()
    {
        return $this->hasMany(Match::class, 'home_team');
    }

    /**
     * Make relation with matches table to get all away matches result from there
     * @return HasMany
     */
    function awayMatches()
    {
        return $this->hasMany(Match::class, 'away_team');
    }


    /**
     * Calculate total played matches
     * @return int
     */
    function getTotalMatchAttribute(): int
    {
        return $this->total_win + $this->total_drawn + $this->total_loss;
    }

    /**
     * Calculate total points gain from matches
     * @return int
     */
    function getTotalPointsAttribute(): int
    {
        return ($this->total_win * 3) + $this->total_drawn;

    }

    /**
     * Get total win played matches
     * @return int
     */
    function getTotalWinAttribute()
    {
        $home = $this->homeMatches()->whereColumn('home_score', '>', 'away_score')->count();
        $away = $this->awayMatches()->whereColumn('home_score', '<', 'away_score')->count();
        return $home + $away;
    }

    /**
     * Get total drawn played matches
     * @return int
     */
    function getTotalDrawnAttribute()
    {

        $home = $this->homeMatches()->whereColumn('home_score', '=', 'away_score')->count();
        $away = $this->awayMatches()->whereColumn('home_score', '=', 'away_score')->count();

        return $home + $away;
    }

    /**
     *  Get total loss played matches
     * @return int
     */
    function getTotalLossAttribute()
    {
        $home = $this->homeMatches()->whereColumn('home_score', '<', 'away_score')->count();
        $away = $this->awayMatches()->whereColumn('home_score', '>', 'away_score')->count();
        return $home + $away;
    }

    /**
     *  Get total loss played matches
     * @return int
     */
    function getGoalsForAttribute()
    {
        $home = $this->homeMatches()->sum('home_score');
        $away = $this->awayMatches()->sum('away_score');
        return $home + $away;
    }

    /**
     *  Get total loss played matches
     * @return int
     */
    function getGoalsAgainstAttribute()
    {
        $home = $this->homeMatches()->sum('away_score');
        $away = $this->awayMatches()->sum('home_score');
        return $home + $away;
    }
}
