<?php

namespace App\Http\Controllers;

use App\Contracts\MatchInterface;
use App\Contracts\SimulationInterface;
use App\Contracts\TeamInterface;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @var TeamInterface
     */
    private $team;
    /**
     * @var MatchInterface
     */
    private $match;
    /**
     * @var SimulationInterface
     */
    private $simulation;

    /**
     * HomeController constructor.
     * @param TeamInterface $team
     * @param MatchInterface $match
     * @param SimulationInterface $simulation
     */
    public function __construct(TeamInterface $team, MatchInterface $match, SimulationInterface $simulation)
    {
        $this->team = $team;
        $this->match = $match;
        $this->simulation = $simulation;
    }


    /**
     * Load view and show results for user
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('welcome')
            ->with('teams', $this->team->all()->sortByDesc('total_points'))
            ->with('currentMatches', $this->match->currentWeek())
            ->with('matches', $this->match->all()->sortByDesc('week_number'));
    }

    /**
     * Play one week
     * @return array
     */
    public function nextWeek()
    {
        return ['success' => $this->simulation->playWeek()];
    }

    /**
     * Play all weeks
     * @return array
     */
    public function playAll()
    {
        return ['success' => $this->simulation->playAll()];
    }

    public function updateMatch(Request $request){
        return ['success' => $this->match->update($request->all())];
    }

}
