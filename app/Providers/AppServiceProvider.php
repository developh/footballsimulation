<?php

namespace App\Providers;

use App\Contracts\MatchInterface;
use App\Contracts\SimulationInterface;
use App\Contracts\TeamInterface;
use App\Repository\MatchRepo;
use App\Repository\SimulationRepo;
use App\Repository\TeamRepo;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TeamInterface::class,TeamRepo::class);
        $this->app->bind(MatchInterface::class, MatchRepo::class);
        $this->app->bind(SimulationInterface::class, SimulationRepo::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
