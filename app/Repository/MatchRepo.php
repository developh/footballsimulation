<?php


namespace App\Repository;


use App\Contracts\MatchInterface;
use App\Entities\Match;


class MatchRepo implements MatchInterface
{
    /**
     * @var Match
     */
    private $match;

    /**
     * TeamRepo constructor.
     * @param Match $match
     */
    function __construct(Match $match)
    {
        $this->match = $match;
    }

    public function all()
    {
        return $this->match->get();
    }


    public function currentWeek(){
        return $this->match->whereNotNull('home_team')
            ->whereNotNull('away_score')
            ->orderBy('week_number','desc')
            ->limit(2)
            ->get();
    }

    /**
     * Update match
     * @param array $data
     * @return bool
     */
    public function update($data = [])
    {

        if(isset($data['id'])){
            return $this->match->find($data['id'])->update($data);
        }

        return false;
    }
}
