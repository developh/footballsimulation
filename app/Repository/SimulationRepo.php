<?php


namespace App\Repository;


use App\Contracts\SimulationInterface;
use App\Entities\Match;
use App\Entities\Team;

class SimulationRepo implements SimulationInterface
{
    /**
     * @var Team
     */
    private $team;
    /**
     * @var Match
     */
    private $match;

    /**
     * SimulationRepo constructor.
     * @param Team $team
     * @param Match $match
     */
    public function __construct(Team $team, Match $match)
    {
        $this->team = $team;
        $this->match = $match;
    }

    /**
     * Play one week and save results
     * @return bool
     */
    public function playWeek()
    {

        $teams = $this->team->get();
        $stop = 0;
        $weekNumber = $this->match->max('week_number') + 1;
        $totalMatches = $this->getNumberOfMatch() / 2;

        if ($weekNumber <= $totalMatches) {
            foreach ($teams as $team) {
                $homeMatch = $team->homeMatches()->pluck('away_team')->toArray();
                $homeMatch[]=$team->id;
                $teamToPlay = $this->team->whereNotIn('id', $homeMatch)->inRandomOrder()->first();


                if ($teamToPlay) {
                    $results = $this->play($team, $teamToPlay);


                    $this->match->create([
                            'home_team' => $team->id,
                            'away_team' => $teamToPlay->id,
                            'home_score' => $results['teamGoals'],
                            'away_score' => $results['teamToPlayGoals'],
                            'week_number' => $weekNumber,
                        ]

                    );
                    $stop++;
                }else{
                    $awayMatch = $team->awayMatches()->pluck('home_team')->toArray();
                    $awayMatch[]=$team->id;
                    $teamToPlay = $this->team->whereNotIn('id', $awayMatch)->inRandomOrder()->first();

                    if ($teamToPlay) {
                        $results = $this->play($team, $teamToPlay);


                        $this->match->create([
                                'home_team' => $teamToPlay->id,
                                'away_team' => $team->id,
                                'home_score' => $results['teamGoals'],
                                'away_score' => $results['teamToPlayGoals'],
                                'week_number' => $weekNumber,
                            ]

                        );
                        $stop++;
                    }
                }
                if ($stop == 2) {
                    break;
                }
            }
        }

        return true;
    }

    /**
     * Play one week and save results
     * @return bool
     */
    public function playAll()
    {
        $totalMatches = $this->getNumberOfMatch() / 2;

        for ($i = 0; $i < $totalMatches; $i++) {
            $this->playWeek();
        }
        return true;
    }

    /**
     * return int
     */
    private function getNumberOfMatch()
    {
        $totalTeams = $this->team->count('name');
        $totalMatches = 1;
        for ($i = 2; $i <= $totalTeams; $i++) {
            $totalMatches = $i * $totalMatches;
        }

        $remain = 1;
        for ($i = 2; $i < $totalTeams - 2; $i++) {
            $remain = $i * ($totalTeams - 2);
        }
        return $totalMatches / (2 * $remain);
    }

    /**
     * @param Team $team
     * @param Team $teamToPlay
     * @return array
     */
    private function play($team, $teamToPlay)
    {
        $teamWinPercent = round($team->total_points * 100 / $this->team->get()->sum('total_points'));
        $teamToPlayWinPercent = round($teamToPlay->total_points * 100 / $this->team->get()->sum('total_points'));
        $teamGoals = 0;
        $teamToPlayGoals = 0;

        if ($teamWinPercent < $teamToPlayWinPercent) {
            $teamGoals = rand(0, 3);
            $teamToPlayGoals = rand(4, 7);
        }
        if ($teamWinPercent > $teamToPlayWinPercent) {
            $teamGoals = rand(4, 7);
            $teamToPlayGoals = rand(0, 3);
        }

        if ($teamWinPercent > $teamToPlayWinPercent) {
            $teamGoals = rand(0, 7);
            $teamToPlayGoals = $teamGoals;
        }

        return [
            'teamGoals' => $teamGoals,
            'teamToPlayGoals' => $teamToPlayGoals
        ];
    }
}
