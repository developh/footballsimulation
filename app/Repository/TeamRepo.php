<?php


namespace App\Repository;


use App\Contracts\TeamInterface;
use App\Entities\Team;
use Illuminate\Database\Eloquent\Collection;

class TeamRepo implements TeamInterface
{
    /**
     * @var Team
     */
    private $team;

    /**
     * TeamRepo constructor.
     * @param Team $team
     */
    function __construct(Team $team)
    {
        $this->team = $team;
    }

    /**
     * @return Team[]|Collection
     */
    function all()
    {
        return $this->team->get();
    }
}
