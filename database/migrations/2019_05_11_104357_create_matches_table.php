<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('home_team');
            $table->unsignedSmallInteger('home_score')->nullable();
            $table->foreign('home_team')
                ->references('id')->on('teams')
                ->onDelete('cascade');

            $table->unsignedBigInteger('away_team');
            $table->unsignedSmallInteger('away_score')->nullable();
            $table->foreign('away_team')
                ->references('id')->on('teams')
                ->onDelete('cascade');
            $table->unsignedSmallInteger('week_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
