<?php


use App\Entities\Match;
use Illuminate\Database\Seeder;

class MatchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET foreign_key_checks=0");
        Match::truncate();
        DB::statement("SET foreign_key_checks=1");

        Match::insert([
            [
                'home_team' => 1,
                'home_score' => 2,
                'away_team' => 2,
                'away_score' => 4,
                'week_number' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],[
                'home_team' => 3,
                'home_score' => 2,
                'away_team' => 4,
                'away_score' => 1,
                'week_number' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
