<?php

use App\Entities\Team;
use Illuminate\Database\Seeder;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {

        DB::statement("SET foreign_key_checks=0");
        Team::truncate();
        DB::statement("SET foreign_key_checks=1");

        Team::insert([
            ['name' => 'Manchester City','created_at'=>now(),'updated_at'=>now()],
            ['name' => 'Liverpool','created_at'=>now(),'updated_at'=>now()],
            ['name' => 'Chelsea','created_at'=>now(),'updated_at'=>now()],
            ['name' => 'Arsenal','created_at'=>now(),'updated_at'=>now()],
        ]);
    }
}
