## Step to run project
`cp .env.example .env`

`Edit database connection in .env file`

`composer install`

`chmod -R 777 storage/ bootstrap/cache/`

`php artisan migrate`

`composer dumpautoload -o`

`php artisan db:seed`

