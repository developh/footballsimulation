let simulation = function () {
    return {
        //== Public functions
        init: function () {
           $("#next-week").click(function () {
               $.ajax({
                   url:"/next-week"
               }).done(function (data) {
                   if(data.success){
                       alert("Week game is finished");
                       window.location.reload();
                   }
               })
           });
           $("#play-all").click(function () {
               $.ajax({
                   url:"/play-all"
               }).done(function (data) {
                   if(data.success){
                       alert("League game is finished");
                       window.location.reload();
                   }

               })
           });

           $("#matches").change(function () {
               let selectOption=$(this).find("option:selected");
               if(selectOption.val()!==''){
                   $("#raw-inputs").removeClass('d-none');
                   $("#home-label").text(selectOption.attr('data-home-name'));
                   $("#away-label").text(selectOption.attr('data-away-name'));
                   $("#away_score").val(selectOption.attr('data-away-score'));
                   $("#home_score").val(selectOption.attr('data-home-score'));
               }else {
                   $("#raw-inputs").addClass('d-none');
                   $("#home-label").text('');
                   $("#away-label").text('');
                   $("#away_score").val('');
                   $("#home_score").val('');
               }
           });
            $('#edit-match').on('hide.bs.modal', function () {
                $("#matches").val('');
                $("#raw-inputs").addClass('d-none');
                $("#home-label").text('');
                $("#away-label").text('');
                $("#away_score").val('');
                $("#home_score").val('');
            });

            $('#update-match').submit(function (e) {
                e.preventDefault();
                let selectOption=$(this).find("option:selected");
                if(selectOption.val()!=='') {
                    $.ajax({
                        url: "/update-match",
                        type: "post",
                        data: $('#update-match').serialize()
                    }).done(function (data) {
                        if (data.success) {
                            alert("Match updated");
                            window.location.reload();
                        }else {
                            alert("Match not updated please try again");
                        }

                    })
                }else {
                    alert('Please select match to update');
                }

            });
        }
    };

}();
jQuery(document).ready(function () {
    simulation.init();
});


