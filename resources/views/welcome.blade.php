
<?php
    /**
     * @var \App\Entities\Team[] $teams
     * @var \App\Entities\Match[] $matches
     */
?>
    <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">


</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Football Simulation</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">

    </div>
</nav>
<div class="container-fluid">
    <div class="row" style="padding-top: 50px">
        <div class="col-12">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th scope="col" class="text-center">League Table</th>
                    <th scope="col"  class="text-center">Match Results</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="padding: 0;">
                        <table class="table table-striped" style="margin: 0;">
                            <thead>
                            <tr>
                                <th scope="col">Teams</th>



                                <th scope="col">Played</th>
                                <th scope="col">Won</th>
                                <th scope="col">Drawn</th>
                                <th scope="col">Lost</th>
                                <th scope="col">GF</th>
                                <th scope="col">GA</th>
                                <th scope="col">GD</th>
                                <th scope="col">Points</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($teams as $team)
                            <tr>
                                <th scope="row">{{$team->name}}</th>
                                <td>{{$team->total_match}}</td>
                                <td>{{$team->total_win}}</td>
                                <td>{{$team->total_drawn}}</td>
                                <td>{{$team->total_loss}}</td>
                                <td>{{$team->goals_for}}</td>
                                <td>{{$team->goals_against}}</td>
                                <td>{{$team->goals_for - $team->goals_against}}</td>
                                <td>{{$team->total_points}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                    <td style="padding: 0;">
                        <table class="table table-striped" style="margin: 0;">
                            <thead>
                            <tr>
                                <th scope="col" colspan="3" class="text-center">{{$currentMatches[0]->week_number??'0'}}th Week Match Result</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($currentMatches as $currentMatch)
                            <tr class="text-center">
                                <td>{{$currentMatch->home->name}}</td>
                                <td>{{$currentMatch->home_score}} - {{$currentMatch->away_score}}</td>
                                <td>{{$currentMatch->away->name}}</td>
                            </tr>
                            @endforeach
                           <tr class="text-center">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr><tr class="text-center">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>

                </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th scope="col" colspan="2" class="text-center">
                            <button id="play-all" type="button" class="btn btn-primary btn-sm float-left">Play All</button>
                            <button id="play-all" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#edit-match">Edit Week</button>
                            <button id="next-week" type="button" class="btn btn-secondary btn-sm float-right">Next Week</button>
                        </th>

                    </tr>
                </tfoot>
            </table>
        </div>

    </div>

</div>
<div class="modal fade" id="edit-match" tabindex="-1" role="dialog" aria-labelledby="editMatch" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="form" id="update-match">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Match</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @csrf
                <div class="form-group">
                    <label for="matches">Matches</label>
                    <select class="form-control" name="id" id="matches">
                        <option value="" selected>Select Match</option>
                        @foreach($matches as $match)
                            <option value="{{$match->id}}" data-home-name="{{$match->home->name}}" data-away-name="{{$match->away->name}}" data-home-score="{{$match->home_score}}" data-away-score="{{$match->away_score}}">
                                {{"{$match->home->name} vs {$match->away->name}"}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="row d-none" id="raw-inputs">
                    <div class="col-6">
                        <div class="form-group mb-2">
                            <label for="home_score" id="home-label"></label>
                            <input type="text" class="form-control" id="home_score" name="home_score" placeholder="Password">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group mb-2">
                            <label for="away_score" id="away-label"></label>
                            <input type="text" class="form-control" id="away_score" name="away_score" placeholder="Password">
                        </div>
                    </div>

                </div>


            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        </form>
    </div>
</div>
</body>
<script src="{{asset('js/app.js?v0.1')}}"></script>
</html>
